import 'package:flutter/material.dart';
import 'review.dart';

class ReviewList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        new Review("assets/img/people.jpg", 'Ron', 'Musician', "That's ok"),
        new Review("assets/img/people.jpg", 'Ron1', 'Musician', "That's ok"),
        new Review("assets/img/people.jpg", 'Ron2', 'Musician', "That's ok")
      ],
    );
  }

}